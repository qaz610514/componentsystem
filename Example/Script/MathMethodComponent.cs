﻿namespace com.FunJimChee.ComponentSystem.Example
{
    public sealed class MathMethodComponent : ExampleComponentBase
    {
        public float Add(float number1, float number2)
        {
            return number1 + number2;
        }

        public float Sub(float number1, float number2)
        {
            return number1 - number2;
        }
    }
}