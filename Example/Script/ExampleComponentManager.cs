﻿namespace com.FunJimChee.ComponentSystem.Example
{
    public sealed class ExampleComponentManager : ComponentManagerBase<ExampleComponentBase>
    {
        protected override void Init()
        {
            foreach (var component in Components)
            {
                //Call Component Setting() => reference ComponentManager
                component.Value.SettingManager(this);
            }
        }
    }
}