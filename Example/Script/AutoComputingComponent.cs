﻿namespace com.FunJimChee.ComponentSystem.Example
{
    public sealed class AutoComputingComponent : ExampleComponentBase
    {
        private ValueComponent _valueComponent;

        private MathMethodComponent _methodComponent;

        protected override void OnStart()
        {
            _valueComponent = Manager.GetComponent<ValueComponent>();

            _methodComponent = Manager.GetComponent<MathMethodComponent>();
        }

        public float AddResult()
        {
            var n1 = _valueComponent.number1;

            var n2 = _valueComponent.number2;

            return _methodComponent.Add(n1, n2);
        }

        public float SubResult()
        {
            var n1 = _valueComponent.number1;

            var n2 = _valueComponent.number2;

            return _methodComponent.Sub(n1, n2);
        }
    }
}