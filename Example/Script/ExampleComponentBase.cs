﻿namespace com.FunJimChee.ComponentSystem.Example
{
    public abstract class ExampleComponentBase : ComponentBase
    {
        protected ExampleComponentManager Manager;

        public void SettingManager(ExampleComponentManager manager)
        {
            Manager = manager;
        }
    }
}