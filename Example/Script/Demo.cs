﻿namespace com.FunJimChee.ComponentSystem.Example
{
    using System;
    using UnityEngine;

    public class Demo : MonoBehaviour
    {
        private ExampleComponentManager Manager = new ExampleComponentManager();
        
        private void Awake()
        {
            Manager.StartWork();
        }

        // Start is called before the first frame update
        void Start()
        {
            var valueComponent = Manager.GetComponent<ValueComponent>();

            var mathMethodComponent = Manager.GetComponent<MathMethodComponent>();

            var autoComputingComponent = Manager.GetComponent<AutoComputingComponent>();

            valueComponent.number1 = 1;

            valueComponent.number2 = 2;
            
            Debug.Log($"Print by mathMethod => {mathMethodComponent.Add(valueComponent.number1, valueComponent.number2)}");
            
            Debug.Log($"Print by autoComputing => {autoComputingComponent.AddResult()}");
        }
    }   
}
