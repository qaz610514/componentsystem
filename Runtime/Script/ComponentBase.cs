﻿namespace  com.FunJimChee.ComponentSystem
{
    public abstract class ComponentBase
    {
        protected virtual void OnAwake()
        {
            
        }

        protected virtual void OnStart()
        {
            
        }
    }
}