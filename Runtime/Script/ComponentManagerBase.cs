﻿using System.Reflection;

namespace com.FunJimChee.ComponentSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class ComponentManagerBase<T> where T : ComponentBase
    {
        protected Dictionary<Type, T> Components;

        public void StartWork()
        {
            //Init Dictionary
            Components = new Dictionary<Type, T>();

            var types = GetType().Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(T)) && !t.IsAbstract);

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type) as T;

                if (instance == null)
                {
                    continue;
                }

                Components.Add(type, instance);
            }

            Init();

            foreach (var component in Components)
            {
                //Call Component OnAwake()
                
                var method = component.Key.GetMethod("OnAwake", BindingFlags.NonPublic | BindingFlags.Instance);
                
                method?.Invoke(component.Value, new object[0]);
            }

            foreach (var component in Components)
            {
                //Call Component OnStart()
                
                var method = component.Key.GetMethod("OnStart", BindingFlags.NonPublic | BindingFlags.Instance);
                
                method?.Invoke(component.Value, new object[0]);
            }
        }

        protected virtual void Init()
        {
        }

        public TC GetComponent<TC>() where TC : T
        {
            if (Components.ContainsKey(typeof(TC)))
            {
                return Components[typeof(TC)] as TC;
            }

            return default;
        }
    }
}